FROM java:8

WORKDIR /
COPY target/spark2-challenge-1.0-SNAPSHOT-jar-with-dependencies.jar spark2-challenge-fatjar.jar
COPY target/classes/datasets datasets/
COPY start.sh start.sh
RUN chmod +x start.sh

CMD ["/bin/bash"]
