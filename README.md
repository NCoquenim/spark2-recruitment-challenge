# Spark 2 - Recruitment Challenge #

### How to run the Spark program? ###

* Download the docker: `docker pull ncoquenim/spark2-challenge:latest`
* Run container: `docker run -it --rm ncoquenim/spark2-challenge:latest`
* Start Spark program: `./start.sh`

### Where are the csv and parquet files? ###

After running the Spark program, the files will be created at the root:

* `/best_apps`
* `/googleplaystore_cleaned`
* `/googleplaystore_metrics`

### Author ###

* Nelson Coquenim