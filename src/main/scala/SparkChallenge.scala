import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

import udfs.Udfs._

object SparkChallenge {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Spark 2 Challenge")
      .master("local")
      .getOrCreate()

    val appsDf = spark.read
      .options(Map(
        "header" -> "true", "inferSchema" -> "true", "escape" -> "\""))
      .csv("datasets/googleplaystore.csv")
      .withColumn("Reviews", col("Reviews").cast("Long"))

    val reviewsDf = spark.read
      .options(Map("header" -> "true", "inferSchema" -> "true", "escape" -> "\""))
      .csv("datasets/googleplaystore_user_reviews.csv")

    //Part 1 - Apps Average Sentiment Polarity
    val df_1 = computePart1DataFrame(reviewsDf)

    //Part 2 -  Apps with "Rating" greater or equal to 4.0 sorted in descending order.
    computePart2DataFrame(appsDf)
      .write.mode("overwrite")
      .options(Map("sep" -> "§", "escape" -> "\""))
      .csv("best_apps")

    //Part 3
    val df_2 = computePart3DataFrame(appsDf)

    // Part 4 - df_2 join df_1
    val df_3 = df_2.join(df_1, Seq("App"), "left")
    df_3.write.mode("overwrite")
      .option("compression", "gzip")
      .parquet("googleplaystore_cleaned")

    //Part 5
    val df_4 = computePart5DataFrame(df_3)
    df_4.write.mode("overwrite")
      .option("compression", "gzip")
      .parquet("googleplaystore_metrics")
  }


  private def computePart1DataFrame(reviews: DataFrame): DataFrame = {
    reviews
      .groupBy(reviews("App"))
      .agg(
        avg(reviews("Sentiment_Polarity")) as "Average_Sentiment_Polarity"
      )
      .na.fill(0, Seq("Average_Sentiment_Polarity"))
  }

  private def computePart2DataFrame(apps: DataFrame): DataFrame = {
    apps
      .filter("Rating > 4.0")
      .filter("Rating <= 5.0")
      .sort(desc("Rating"))
  }

  private def computePart3DataFrame(apps: DataFrame): DataFrame = {
    val mostReviewedApps = apps
      .groupBy("App")
      .agg(max("Reviews") as "Reviews")

    val appWithCategories = apps
      .groupBy(apps("App"))
      .agg(
        collect_set("Category") as "Categories",
      )
    val temp = appWithCategories
      .join(mostReviewedApps, Seq("App"), "inner")
      .join(apps.drop("Category"), Seq("App", "Reviews"), "inner")
      .dropDuplicates()

    temp.select(temp("App"), temp("Categories"), temp("Rating"),
      temp("Reviews"), temp("Size"), temp("Installs"), temp("Type"), temp("Price"), temp("Content Rating") as "Content_Rating",
      temp("Genres"), temp("Last Updated") as "Last_Updated", temp("Current Ver") as "Current_Version",
      temp("Android Ver") as "Minimum_Android_Version"
    )
      .withColumn("Genres", udfSplit(col("Genres")))
      .withColumn("Genres", udfRemoveDuplicates(col("Genres")))
      .withColumn("Size", udfToMB(col("Size")))
      .withColumn("Price", udfToEuros(col("Price")))
      .withColumn("Last_Updated", to_timestamp(col("Last_Updated"), "MMMM d, yyyy"))
  }

  private def computePart5DataFrame(dataFrame_3: DataFrame): DataFrame = {
    dataFrame_3
      .na.drop(Seq("Rating"))
      .select(explode(col("Genres")) as "Genre", col("Rating"), col("Average_Sentiment_Polarity"))
      .groupBy(col("Genre"))
      .agg(count("Genre") as "Count", avg("Rating") as "Average_Rating",
        avg("Average_Sentiment_Polarity") as "Average_Sentiment_Polarity")
  }
}