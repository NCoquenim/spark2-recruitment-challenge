package udfs

import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf

import scala.collection.mutable
import scala.util.Try

object Udfs {

  private val removeDuplicates: mutable.WrappedArray[String] => mutable.WrappedArray[String] = _.distinct
  val udfRemoveDuplicates: UserDefinedFunction = udf(removeDuplicates)

  def udfSplit: UserDefinedFunction = udf(
    (input: String) => {
      input.split(";")
    }
  )

  def udfToMB: UserDefinedFunction = udf(
    (input: String) => {
      val split = input.splitAt(input.length - 1)
      val value = tryToDouble(split._1)
      val magnitude = split._2

      magnitude match {
        case "M" => value.get
        case "k" => value.get / 1024
        case _ => Double.NaN
      }
    }
  )


  def udfToEuros: UserDefinedFunction = udf(
    (input: String) => {
      val priceInDollars = tryToDouble(input.replace("$", ""))
      priceInDollars.get * 0.9
    }
  )

  private def tryToDouble(s: String): Option[Double] = Try(s.toDouble).toOption
}
